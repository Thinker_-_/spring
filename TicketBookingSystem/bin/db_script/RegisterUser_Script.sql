/* register_user */
create table if not exists register_user (register_user_id integer not null auto_increment, created_at datetime, email varchar(255) not null, name varchar(255) not null, password varchar(255) not null, roleid bigint not null, primary key (register_user_id));

/*  UK_RegisterUser_email */
alter table register_user add constraint UK_RegisterUser_email unique (email);

/*  FK_RegisterUser_roleid */
alter table register_user add constraint FK_RegisterUser_roleid foreign key (roleid) references role (role_id);


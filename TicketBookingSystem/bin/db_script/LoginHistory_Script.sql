/*
 * login_history 
 * 
 * created By Prakhar Verma 
 * Date = 22 August 2017
 * */
create table if not exists login_history (login_history_id integer not null auto_increment, loginip varchar(255), login_time datetime, user_agent varchar(255), userid integer, primary key (login_history_id));

/*  FK_LoginHistory_userid */
alter table login_history add constraint FK_LoginHistory_userid foreign key (userid) references user (u_id);

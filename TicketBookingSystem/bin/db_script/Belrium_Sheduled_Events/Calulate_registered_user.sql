#author prashank jauhari
CREATE EVENT IF NOT EXISTS register_user_history
ON SCHEDULE EVERY '1' day
  STARTS '2017-08-21 00:00:01'
  /*
    This event will start from 12:01 am from deployment date.
    and will be scheduled for each day at 12:01 am
  */

ON COMPLETION PRESERVE
DO

insert into date_table (dates,users) 
select Date(DATE_SUB(NOW(), INTERVAL 1 DAY)) as dates,count(u_id) as users 
from user
where Date(register_date)=Date(DATE_SUB(NOW(), INTERVAL 1 DAY));

/* activity */
create table if not exists activity (activity_id bigint not null auto_increment, active bit not null, activity_description varchar(255), activity_name varchar(255), handler_method_name varchar(255), primary key (activity_id));

/*  UK_Activity_handlerMethodName */
alter table activity add constraint UK_Activity_handlerMethodName unique (handler_method_name);

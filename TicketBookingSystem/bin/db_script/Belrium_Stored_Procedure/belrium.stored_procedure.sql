#author prashank jauhari

drop procedure if exists belrium.reg_freq;
delimiter $$

create procedure belrium.reg_freq(IN start_date varchar(10) ,IN end_date varchar(10),IN odr varchar(5))
begin

case
/*
 case to reterive data by month
*/  
 when odr='month' then      
 select sum(users) as users, monthname(dates) as month_name,year(dates) as year from date_table where 
 dates >=start_date and dates<=end_date
 group by year(dates),month(dates); 

/*
 case to reterive data by day
*/  

 when odr='day' then
 select users,dates from date_table where 
 dates >=start_date and dates<=end_date
 group by dates;


/*
 case to reterive data by year
*/  

 when odr='year' then
 
 select sum(users) as users, year(dates) as year from date_table where 
 dates >=start_date and dates<=end_date
 group by year(dates);

 else select "Something went wrong";
 
end case; 
end $$
delimiter ;

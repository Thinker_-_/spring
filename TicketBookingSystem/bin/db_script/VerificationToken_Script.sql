/* verification_token */

/*
  Last Commit Date 4 September
  
  Add creation date column in verification_token 
  on Date 5 September 2017 
*/

alter table verification_token add creation_date datetime not null;
/* -------------------------------------------------------------- */
create table if not exists verification_token (t_id integer not null auto_increment, deleted bit, token varchar(255) not null, register_user_id integer, u_id integer, primary key (t_id));

/*  FK_VerificationToken_register_user_id */
alter table verification_token add constraint FK_VerificationToken_register_user_id foreign key (register_user_id) references register_user(register_user_id) ON DELETE CASCADE;

/*  FK_VerificationToken_uid */
alter table verification_token add constraint FK_VerificationToken_uid foreign key (u_id) references user (u_id);

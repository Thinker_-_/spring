/**
 * created by Prakhar Verma
 * 26 sepetember 2017
 */

/* kycdocument_type */
create table if not exists kycdocument_type (kyc_document_typeid integer not null auto_increment, document_type varchar(255), country_countryid integer, primary key (kyc_document_typeid))


/*   FK_KYCDocumentType_countryid*/
alter table kycdocument_type add constraint FK_KYCDocumentType_countryid foreign key (country_countryid) references country (countryid)

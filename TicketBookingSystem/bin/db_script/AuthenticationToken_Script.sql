/* authentication_token */
create table if not exists authentication_token (a_id integer not null auto_increment, creation_date datetime, deleted bit, token varchar(255), u_id integer, primary key (a_id));

/*  FK_AuthenticationToken_uid */
alter table authentication_token add constraint FK_AuthenticationToken_uid foreign key (u_id) references user (u_id);

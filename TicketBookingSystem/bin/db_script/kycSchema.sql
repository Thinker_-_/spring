use belrium;
/* 1. activity */
create table activity (activity_id bigint not null auto_increment, active bit not null, activity_description varchar(255), activity_name varchar(255), handler_method_name varchar(255), primary key (activity_id));
/* 2. authentication_token */
create table authentication_token (a_id integer not null auto_increment, creation_date datetime, deleted bit, token varchar(255), u_id integer, primary key (a_id));
/* 3. country */
create table country (countryid integer not null auto_increment, country_code varchar(255), country_name varchar(255), primary key (countryid));
/* 4. kycdocument_meta */
create table kycdocument_meta (kyc_document_metaid integer not null auto_increment, description varchar(255), document_name varchar(255), is_mandatory bit not null, primary key (kyc_document_metaid));
/* 5. kycdocument_type */
create table kycdocument_type (kyc_document_typeid integer not null auto_increment, document_type varchar(255), country_countryid integer, primary key (kyc_document_typeid));
/* 6. kycdocument_type_kycdocument_meta */
create table kycdocument_type_kycdocument_meta (kyc_document_typeid integer not null, kyc_document_metaid integer not null);
/* 7. kycuser_document */
create table kycuser_document (kyc_user_documentid integer not null auto_increment, verifed_at datetime, document_status varchar(255), uploaded_at datetime, url varchar(255), verified_by varchar(255), kyc_document_metaid integer not null, userid integer, primary key (kyc_user_documentid));
/* 8. login_history */
create table login_history (login_history_id integer not null auto_increment, loginip varchar(255), login_time datetime, user_agent varchar(255), userid integer, primary key (login_history_id));
/* 9. register_user */
create table register_user (register_user_id integer not null auto_increment, created_at datetime, email varchar(255) not null, name varchar(255) not null, password varchar(255) not null, roleid bigint not null, primary key (register_user_id));
/* 10. reset_token */
create table reset_token (a_id integer not null auto_increment, creation_date datetime, deleted bit, token varchar(255), u_id integer, primary key (a_id));
/* 11. role */
create table role (role_id bigint not null auto_increment, active bit not null, role_description varchar(255), role_name varchar(255) not null, primary key (role_id));
/* 12. role_activities */
create table role_activities (roles_role_id bigint not null, activities_activity_id bigint not null, primary key (roles_role_id, activities_activity_id));
/* 13. user */
create table user (u_id integer not null auto_increment, address_line1 varchar(255), address_line2 varchar(255), city varchar(255), country varchar(255), phone varchar(255), pincode varchar(255), state varchar(255), email varchar(255) not null, is_active bit, isemailverified bit, iskyc_verified bit, name varchar(255) not null, password varchar(255) not null, profile_image_path varchar(255), register_date datetime, username varchar(255) not null, roleid bigint not null, primary key (u_id));
/* 14. verification_token */
create table verification_token (t_id integer not null auto_increment, deleted bit, token varchar(255) not null, register_user_id integer, u_id integer,creation_date datetime, primary key (t_id));
/* 15. wallet */
create table wallet (wallet_id integer not null auto_increment, wallet_status varchar(255), wallet_address varchar(255), u_id integer, primary key (wallet_id));
/* 16. date_table */
create table date_table (dates date primary key,users int default 0);

/********************* Constraints ******************************/

/* 1.  UK_Activity_handlerMethodName */
alter table activity add constraint UK_Activity_handlerMethodName unique (handler_method_name);
/* 2.  UK_country_countryName */
alter table country add constraint UK_countryName unique (country_name);
/* 3.  UK_RegisterUser_email */
alter table register_user add constraint UK_RegisterUser_email unique (email);
/* 3.  UK_Role_roleName */
alter table role add constraint UK_Role_roleName unique (role_name);
/* 4.  UK_User_email */
alter table user add constraint UK_User_email unique (email);
/* 5.  FK_AuthenticationToken_uid */
alter table authentication_token add constraint FK_AuthenticationToken_uid foreign key (u_id) references user (u_id);
/* 7.  FK_KYCDocumentType_countryid*/
alter table kycdocument_type add constraint FK_KYCDocumentType_countryid foreign key (country_countryid) references country (countryid);
/* 8.  FK_KYCDocumentTypeKYCDocumentMeta_kycDocumentMetaid*/
alter table kycdocument_type_kycdocument_meta add constraint FK_KYCDocumentTypeKYCDocumentMeta_kycDocumentMetaid foreign key (kyc_document_metaid) references kycdocument_meta (kyc_document_metaid);
/* 9.  FK_KYCDocumentTypeKYCDocumentMeta_kycDocumentTypeid*/
alter table kycdocument_type_kycdocument_meta add constraint FK_KYCDocumentTypeKYCDocumentMeta_kycDocumentTypeid foreign key (kyc_document_typeid) references kycdocument_type (kyc_document_typeid);
/* 10.  FK_KYCUserDocument_kycDocumentMetaid*/
alter table kycuser_document add constraint FK_KYCUserDocument_kycDocumentMetaid foreign key (kyc_document_metaid) references kycdocument_meta (kyc_document_metaid);
/* 11.  FK_KYCUserDocument_userid*/
alter table kycuser_document add constraint FKkqkvsj7djpgyebwhpdiqjb8pr foreign key (userid) references user (u_id);
/* 12.  FK_LoginHistory_userid */
alter table login_history add constraint FK_LoginHistory_userid foreign key (userid) references user (u_id);
/* 13.  FK_RegisterUser_roleid */
alter table register_user add constraint FK_RegisterUser_roleid foreign key (roleid) references role (role_id);
/* 14.  FK_ResetToken_uid */
alter table reset_token add constraint FK_ResetToken_uid foreign key (u_id) references user (u_id);
/* 15.  FK_RoleActivities_activityId */
alter table role_activities add constraint FK_RoleActivities_activityId foreign key (activities_activity_id) references activity (activity_id);
/* 16. FK_RoleActivities_roleId */
alter table role_activities add constraint FK_RoleActivities_roleId foreign key (roles_role_id) references role (role_id);
/* 17. FK_User_roleId */
alter table user add constraint FK_User_roleId foreign key (roleid) references role (role_id);
/* 18. FK_VerificationToken_register_user_id */
alter table verification_token add constraint FK_VerificationToken_register_user_id foreign key (register_user_id) references register_user(register_user_id) ON DELETE CASCADE;
/* 19. FK_VerificationToken_uid */
alter table verification_token add constraint FK_VerificationToken_uid foreign key (u_id) references user (u_id);
/* 20. FK_Wallet_uid */
alter table wallet add constraint FK_Wallet_uid foreign key (u_id) references user (u_id);


/************************ Procedures *****************************/

/* 1. belrium.stored_procedure */
drop procedure if exists belrium.reg_freq;
delimiter $$

create procedure belrium.reg_freq(IN start_date varchar(10) ,IN end_date varchar(10),IN odr varchar(5))
begin

case
/*
 case to reterive data by month
*/  
 when odr='month' then      
 select sum(users) as users, monthname(dates) as month_name,year(dates) as year from date_table where 
 dates >=start_date and dates<=end_date
 group by year(dates),month(dates); 

/*
 case to reterive data by day
*/  

 when odr='day' then
 select users,dates from date_table where 
 dates >=start_date and dates<=end_date
 group by dates;


/*
 case to reterive data by year
*/  

 when odr='year' then
 
 select sum(users) as users, year(dates) as year from date_table where 
 dates >=start_date and dates<=end_date
 group by year(dates);

 else select "Something went wrong";
 
end case; 
end $$
delimiter ;



/************************ Events *****************************/


CREATE EVENT IF NOT EXISTS register_user_history
ON SCHEDULE EVERY '1' day
  STARTS '2017-08-21 00:00:01'
  /*
    This event will start from 12:01 am from deployment date.
    and will be scheduled for each day at 12:01 am
  */

ON COMPLETION PRESERVE
DO

insert into date_table (dates,users) 
select Date(DATE_SUB(NOW(), INTERVAL 1 DAY)) as dates,count(u_id) as users 
from user
where Date(register_date)=Date(DATE_SUB(NOW(), INTERVAL 1 DAY));


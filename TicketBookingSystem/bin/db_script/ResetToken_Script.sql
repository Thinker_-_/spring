/* reset_token */
create table if not exists reset_token (a_id integer not null auto_increment, creation_date datetime, deleted bit, token varchar(255), u_id integer, primary key (a_id));

/*  FK_ResetToken_uid */
alter table reset_token add constraint FK_ResetToken_uid foreign key (u_id) references user (u_id);

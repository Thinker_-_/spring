/* user */
create table if not exists user (u_id integer not null auto_increment, address_line1 varchar(255), address_line2 varchar(255), city varchar(255), country varchar(255), phone varchar(255), pincode varchar(255), state varchar(255), email varchar(255) not null, is_active bit, isemailverified bit, iskyc_verified bit, name varchar(255) not null, password varchar(255) not null, profile_image_path varchar(255), register_date datetime, username varchar(255) not null, roleid bigint not null, primary key (u_id));

/*  UK_User_email */
alter table user add constraint UK_User_email unique (email);

/*  FK_User_roleId */
alter table user add constraint FK_User_roleId foreign key (roleid) references role (role_id);

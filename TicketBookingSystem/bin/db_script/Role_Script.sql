/* role */
create table if not exists role (role_id bigint not null auto_increment, active bit not null, role_description varchar(255), role_name varchar(255) not null, primary key (role_id));

/*   UK_Role_roleName */
alter table role add constraint UK_Role_roleName unique (role_name);


/**
 * created by Prakhar Verma
 * 26 sepetember 2017
 */

/* kycdocument_type_kycdocument_meta */
create table kycdocument_type_kycdocument_meta (kyc_document_typeid integer not null, kyc_document_metaid integer not null)

/*   FK_KYCDocumentTypeKYCDocumentMeta_kycDocumentMetaid*/
alter table kycdocument_type_kycdocument_meta add constraint FK_KYCDocumentTypeKYCDocumentMeta_kycDocumentMetaid foreign key (kyc_document_metaid) references kycdocument_meta (kyc_document_metaid)
/*   FK_KYCDocumentTypeKYCDocumentMeta_kycDocumentTypeid*/
alter table kycdocument_type_kycdocument_meta add constraint FK_KYCDocumentTypeKYCDocumentMeta_kycDocumentTypeid foreign key (kyc_document_typeid) references kycdocument_type (kyc_document_typeid)

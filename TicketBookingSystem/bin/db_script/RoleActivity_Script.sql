/* role_activities */
 create table if not exists role_activities (roles_role_id bigint not null, activities_activity_id bigint not null, primary key (roles_role_id, activities_activity_id));

 /*  FK_RoleActivities_activityId */
alter table role_activities add constraint FK_RoleActivities_activityId foreign key (activities_activity_id) references activity (activity_id);

/*  FK_RoleActivities_roleId */
alter table role_activities add constraint FK_RoleActivities_roleId foreign key (roles_role_id) references role (role_id);

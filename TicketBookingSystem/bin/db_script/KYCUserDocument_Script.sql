/**
 * created by Prakhar Verma
 * 26 sepetember 2017
 */

/* kycuser_document */
create table kycuser_document (kyc_user_documentid integer not null auto_increment, verifed_at datetime, document_status varchar(255), uploaded_at datetime, url varchar(255), verified_by varchar(255), kyc_document_metaid integer not null, userid integer, primary key (kyc_user_documentid))

/*   FK_KYCUserDocument_kycDocumentMetaid*/
alter table kycuser_document add constraint FK_KYCUserDocument_kycDocumentMetaid foreign key (kyc_document_metaid) references kycdocument_meta (kyc_document_metaid)
/*   FK_KYCUserDocument_userid*/
alter table kycuser_document add constraint FKkqkvsj7djpgyebwhpdiqjb8pr foreign key (userid) references user (u_id)

/**
 * created by Prakhar Verma
 * 26 sepetember 2017
 */

/* kycdocument_meta */

create table if not exists kycdocument_meta (kyc_document_metaid integer not null auto_increment, description varchar(255), document_name varchar(255), is_mandatory bit not null, primary key (kyc_document_metaid))


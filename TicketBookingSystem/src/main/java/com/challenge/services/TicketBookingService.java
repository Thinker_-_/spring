package com.challenge.services;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.challenge.domain.Screen;
import com.challenge.domain.Seat;
import com.challenge.exceptions.TicketBookingException;
import com.challenge.repository.ScreenRepository;
import com.challenge.repository.SeatRespository;
import com.challenge.util.TicketBookingUtility;

@Service
@SuppressWarnings("unchecked")
public class TicketBookingService {



	@Autowired
	ScreenRepository screenRepository;

	@Autowired
	SeatRespository seatRespository;

	private static Logger logger=LoggerFactory.getLogger(TicketBookingService.class);

	/**
	 * Transaction service method to create 
	 * seating information as per the screen.
	 * @param request
	 */
	@Transactional
	public void createScreenData(Map<String,Object> request){

		Screen screen=new Screen(request.get("name").toString());
		screenRepository.save(screen);
		HashMap<String,Object> seatInfo=(HashMap<String,Object>)request.get("seatInfo");
		List<Seat> seats=new LinkedList<Seat>();
		Set<String> rows=seatInfo.keySet();
		for(String row:rows){
			HashMap<String,Object> rowArrangeMents=(HashMap<String,Object>)seatInfo.get(row);
			Integer totalSeatsInRow=(Integer)rowArrangeMents.get("numberOfSeats");
			List<Integer> aisleSeats=(List<Integer>)rowArrangeMents.get("aisleSeats");
			for(int i=0;i<totalSeatsInRow;i++){
				Seat seat=new Seat("unreserved", row, screen, i, TicketBookingUtility.isAsileSeat(aisleSeats, i));
				seats.add(seat);
			}

			for(Integer aisleSeatNumber: aisleSeats){
				seats.get(aisleSeatNumber).setAisleSeat(true);
			}
		}

		seatRespository.save(seats);
	}

	/**
	 * Transaction method to 
	 * book seat(s)
	 * @param request
	 * @param screenName
	 * @throws TicketBookingException
	 */
	@Transactional
	public void reserveSeat(Map<String,Object> request,String screenName) throws TicketBookingException{
		PageRequest pageRequest=new PageRequest(0, 1);
		Page<Screen> pagescreen=screenRepository.findByName(screenName, pageRequest);
		if(pagescreen.getTotalElements()==0)
			throw new TicketBookingException("Invalid screen");
		Screen screen=pagescreen.getContent().get(0);

		Map<String,Object> seatsInfo=(Map<String,Object>)request.get("seats");

		Set<String> keys=seatsInfo.keySet();
		for(String key:keys){
			List<Integer> seatNumber=(List<Integer>)seatsInfo.get(key);
			List<Seat> seat=seatRespository.findByRowNameAndBookingStatusAndScreenAndSeatNumberIn(key, "unreserved", screen, seatNumber);
			if(seat.isEmpty() ||  seat.size() !=seatNumber.size())
				throw new TicketBookingException("Sorry, Invalid request");


			seatRespository.bookseats("reserved", key, screen.getId(), seatNumber);
		}
	}

	/**
	 * Service method to get seats in ticket db as per screen name and booking status
	 * @param screenName
	 * @param bookingStatus
	 * @return
	 */
	public Map<String,Object> getSeatInformation(String screenName,String bookingStatus){
		PageRequest pageRequest=new PageRequest(0, 1);
		Page<Screen> pagescreen=screenRepository.findByName(screenName, pageRequest);
		if(pagescreen.getTotalElements()==0)
			throw new TicketBookingException("Invalid screen");
		if(!(bookingStatus.equalsIgnoreCase("unreserved") || bookingStatus.equalsIgnoreCase("reserved")))
			throw new TicketBookingException("Invalid booking status");			

		Screen screen=pagescreen.getContent().get(0);
		HashMap<String,Object> response=new HashMap<String,Object>();
		Sort sort = new Sort(new Sort.Order(Direction.ASC, "seatNumber"));
		List<Seat> seats=seatRespository.findByScreenAndBookingStatus(screen, bookingStatus,sort);
		for(Seat seat: seats){
			List<Integer> seatsId=response.get(seat.getRowName())==null?new LinkedList<Integer>():(List<Integer>)response.get(seat.getRowName());	
			seatsId.add(seat.getSeatNumber());
			response.put(seat.getRowName(), seatsId);
		}
		HashMap<String,Object> object=new HashMap<String,Object>();
		object.put("seats", response);

		return object;
	}
}

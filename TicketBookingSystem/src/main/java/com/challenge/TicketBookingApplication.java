package com.challenge;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * Main class of ticket booking application
 * @author Prashank Jauhari
 */
@SpringBootApplication
@EnableAsync
public class TicketBookingApplication  {
	private static final Logger logger = LoggerFactory.getLogger(TicketBookingApplication.class);
	public static void main(String[] args) {
		logger.debug("Starting kyc Managment application");
		SpringApplication.run(TicketBookingApplication.class, args);
	}

}

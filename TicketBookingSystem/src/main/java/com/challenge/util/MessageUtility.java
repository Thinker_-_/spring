package com.challenge.util;

import java.util.Properties;


/**
 * Message Utilty Class for reading
 * SuccessMessage and ErrorMessage
 * @author prashank jauhari
 */

public class MessageUtility {
	
	private static Properties successMessage =new Properties();
	
	private static Properties errorMesssage =new Properties();
	
	
	public MessageUtility(){}
	
	/**
	 * Initialize Success and ErrorMessage from
	 * Properties File	
	 */
	
	public void initialze(){
		loadSuccessMessage();
		loadErrorMessage();
	}

	/**
	 * Return value corresponding to successkey 
	 * if not found return null
	 * @param successkey
	 * @return
	 */
	
	public static String getSuccessMessage(String successkey){
		return (String)successMessage.get(successkey);
	}
	
	/**
	 * Return value corresponding to errorkey 
	 * if not found return null
	 * @param errorkey
	 * @return
	 */
	
	public static String getErrorMessage(String errorkey){
		return (String)errorMesssage.get(errorkey);
	}
	/**
	 * Loading success Messages
	 */
	private void loadSuccessMessage(){
		Properties propertiesObj = null;
		propertiesObj = TicketBookingUtility.propertiesFileReader("assests/message/successMessage.properties");
		if(!TicketBookingUtility.isNull(propertiesObj)){
			successMessage = propertiesObj;
		}
	}
	
    /**
     * 
	 * Loading error Messages
	 */	
	private void loadErrorMessage(){
		Properties propertiesObj = null;
		propertiesObj = TicketBookingUtility.propertiesFileReader("assests/message/errorMessage.properties");
		if(!TicketBookingUtility.isNull(propertiesObj)){
			errorMesssage = propertiesObj;
		}
	}
}

package com.challenge.util;

import java.io.InputStream;
import java.util.List;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.challenge.TicketBookingApplication;

/**
 * Utilites Class for TicketBooking System
 * @author Prashank Jauhari
 * 
 */
public class TicketBookingUtility {

	private static final Logger logger = LoggerFactory.getLogger(TicketBookingUtility.class);

	public static boolean isNull(Object obj){
		return (obj == null)? true : false;
	}
	
	/*
	 *  function for Loading Property file. 
	 */
	public static Properties propertiesFileReader(String filePath){

		Properties prop = null;
		InputStream input = null;
		try{
			input = TicketBookingApplication.class.getClassLoader().getResourceAsStream(filePath);
			prop = new Properties();
			prop.load(input);
		}catch (Exception e) {
			logger.error("Exception in Properties file Reading : "+e.getMessage());
			e.printStackTrace();
			return null;
		}
		return prop;
	}
	
	public static boolean isAsileSeat(List<Integer> seats,int i){
			for(Integer seat:seats){
				if(i==seat)
					return true;
			}
		return false;
	}
}
package com.challenge.exceptions;

public class TicketBookingException extends RuntimeException{
	
	public TicketBookingException(String message){
		super(message);
	}
}

package com.challenge;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * 
 * Configuring reusable singleton 
 * bean for different component used
 * in this ticketing application.
 * @author prashank
 */
@Configuration
public class TickedSystemConfiguration {

	@Bean
	ObjectMapper objectMapper(){
	ObjectMapper myObjectMapper=new ObjectMapper();
		myObjectMapper.setVisibility(PropertyAccessor.FIELD, Visibility.ANY);
		return myObjectMapper;
	}
}

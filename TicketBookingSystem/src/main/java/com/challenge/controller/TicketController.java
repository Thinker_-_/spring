package com.challenge.controller;


import java.util.Map;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.challenge.domain.SeatInfoObject;
import com.challenge.exceptions.TicketBookingException;
import com.challenge.services.TicketBookingService;
import com.challenge.util.ResponseUtil;


/**
 * Rest Controller for ticketing 
 * system specify the end points .
 * @author root
 *
 */
@RestController
public class TicketController {

	private static final Logger logger = LoggerFactory.getLogger(TicketController.class);

	@Autowired
	TicketBookingService ticketBookingService;

	/**
	 * API to create seats information as per the screen.
	 * @param request
	 * @return
	 * @author Prashank Jauhari
	 */
	@PostMapping(value="/screens",produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Object> createSeatInformation(@RequestBody Map<String,Object> request){
		try{
			ticketBookingService.createScreenData(request);
		}catch(Exception e){
			logger.error("error while service the request",e);
			return	ResponseUtil.errorResponse("Sorry, There is some error creating the information please try again later.",HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return ResponseUtil.response(null,"Information Saved Successfully");
	}

	/**
	 * API to book seats in a screen.
	 * @param request
	 * @param screenName
	 * @return
	 */
	@PostMapping(value="/screens/{screenName}/reserve",produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Object> bookSeats(@RequestBody Map<String,Object> request,@PathVariable(value = "screenName") String screenName){

		try{
			logger.info("screen name {} andrequest object {}",screenName,request);

			ticketBookingService.reserveSeat(request, screenName);
		}catch (TicketBookingException e) {
			return	ResponseUtil.errorResponse(e.getMessage(),HttpStatus.BAD_REQUEST);
		}
		catch(Exception e){
			logger.error("error while service the request",e);
			return	ResponseUtil.errorResponse("Sorry, There is some error creating the information please try again later.",HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity<Object>("Booking done successfully", HttpStatus.OK);
	}

	/**
	 * API to fetch the
	 * seats as per its status.
	 * @param credential
	 * @param result
	 * @return
	 * @author Prashank Jauhari
	 */
	@GetMapping(value="screens/{screenName}/seats",produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Object> getAvailableSeats(@Valid @ModelAttribute SeatInfoObject seatObject,BindingResult result){
		try{
			if(result.hasErrors()){
				return new ResponseEntity<Object>(ResponseUtil.fieldErrorResponse("field input not correct", result), HttpStatus.BAD_REQUEST);
			}
			Map<String,Object> object=null;

			object=ticketBookingService.getSeatInformation(seatObject.getScreenName(), seatObject.getStatus());
			return new ResponseEntity<Object>(object, HttpStatus.OK);
		}catch(Exception e){
			logger.error("error while service the request",e);
			return	ResponseUtil.errorResponse(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}


}
package com.challenge.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import com.challenge.domain.Screen;

public interface ScreenRepository extends JpaRepository<Screen,Integer>{
	
	/**
	 * Repository method to find Screen object through its name
	 * @param name
	 * @param page
	 * @return
	 */
	Page<Screen> findByName(String name,Pageable page);
}

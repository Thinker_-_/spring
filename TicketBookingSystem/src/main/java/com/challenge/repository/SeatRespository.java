package com.challenge.repository;

import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.challenge.domain.Screen;
import com.challenge.domain.Seat;

public interface SeatRespository extends JpaRepository<Seat,Integer>{

	/**
	 * Repository method to book a list of seats
	 * @param bookingStatus
	 * @param rowName
	 * @param screenId
	 * @param seatIds
	 * @return
	 */
	@Modifying
	@Query("update Seat s set s.bookingStatus=:bookingStatus where s.rowName=:rowName and s.screen.id=:screenId and s.seatNumber in (:seatids)")
	int bookseats(@Param("bookingStatus")String bookingStatus,@Param("rowName")String rowName,@Param("screenId")Integer screenId,@Param("seatids")List<Integer> seatIds);

	/**
	 * Repository method to find Seat
	 * as per below params.
	 * @param rowname
	 * @param bookingStatus
	 * @param screen
	 * @param seatNumbers
	 * @return
	 */
	List<Seat> findByRowNameAndBookingStatusAndScreenAndSeatNumberIn(String rowname,String bookingStatus,Screen screen,List<Integer> seatNumbers);


	/**
	 * Repository method to find Seat
	 * as per below params and sorting direction.
	 * @param rowname
	 * @param bookingStatus
	 * @param screen
	 * @param seatNumbers
	 * @return
	 */
	List<Seat>  findByScreenAndBookingStatus(Screen screen,String bookingStatus,Sort sort);


	List<Seat>  findByScreenAndBookingStatusAndRowName(Screen screen,String bookingStatus,String rowName,Sort sort);

}

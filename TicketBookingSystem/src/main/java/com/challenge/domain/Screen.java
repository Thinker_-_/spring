package com.challenge.domain;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.OneToMany;

@Entity
public class Screen extends GenericEntity{

	public String name;

	@OneToMany(mappedBy="screen")
	public List<Seat> seats;

	public Screen(){}

	public Screen(String name){
		this.name=name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Seat> getSeats() {
		return seats;
	}

	public void setSeats(List<Seat> seats) {
		this.seats = seats;
	}


}

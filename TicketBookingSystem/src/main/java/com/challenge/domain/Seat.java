package com.challenge.domain;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
public class Seat extends GenericEntity{
	
	private String bookingStatus;
	
	private String rowName;
	
	@ManyToOne
    private	Screen screen;
    
    private Integer seatNumber;

    private boolean aisleSeat;
    
    public Seat(){
    }
    public Seat(String bookingStatus,String rowName,Screen screen,Integer seatNumber,boolean aisleSeat){
    	this.bookingStatus=bookingStatus;
    	this.rowName=rowName;
    	this.screen=screen;
    	this.seatNumber=seatNumber;
    	this.aisleSeat=aisleSeat;
    }


	public boolean isAisleSeat() {
		return aisleSeat;
	}
	public void setAisleSeat(boolean aisleSeat) {
		this.aisleSeat = aisleSeat;
	}
	public Integer getSeatNumber() {
		return seatNumber;
	}

	public void setSeatNumber(Integer seatNumber) {
		this.seatNumber = seatNumber;
	}

	public String getBookingStatus() {
		return bookingStatus;
	}

	public void setBookingStatus(String bookingStatus) {
		this.bookingStatus = bookingStatus;
	}

	public String getRowName() {
		return rowName;
	}

	public void setRowName(String rowName) {
		this.rowName = rowName;
	}

	public Screen getScreen() {
		return screen;
	}

	public void setScreen(Screen screen) {
		this.screen = screen;
	}
	
    
}

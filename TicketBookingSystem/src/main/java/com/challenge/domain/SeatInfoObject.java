package com.challenge.domain;

import org.hibernate.validator.constraints.NotEmpty;

public class SeatInfoObject {

	@NotEmpty(message="screenName is required")
	String ScreenName;
	
	
	String status;
	
	Integer numSeats;
	
	String choice;
	
	

	public Integer getNumSeats() {
		return numSeats;
	}

	public void setNumSeats(Integer numSeats) {
		this.numSeats = numSeats;
	}

	public String getChoice() {
		return choice;
	}

	public void setChoice(String choice) {
		this.choice = choice;
	}

	public String getScreenName() {
		return ScreenName;
	}

	public void setScreenName(String screenName) {
		ScreenName = screenName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	
}

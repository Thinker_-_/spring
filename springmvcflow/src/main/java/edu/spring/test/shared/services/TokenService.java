package edu.spring.test.shared.services;

import java.util.UUID;

public class TokenService {
	
	private String containerName;
	
	
	public String getContainerName() {
		return containerName;
	}

	public void setContainerName(String containerName) {
		this.containerName = containerName;
	}

	public String getToken(){
		return UUID.randomUUID().toString();
	}
	
	public void init(){
		System.out.println("Token service started");
	}

}

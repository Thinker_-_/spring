package edu.spring.test.contexts.normal.exceptions;

import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.AbstractHandlerExceptionResolver;

import com.fasterxml.jackson.databind.ObjectMapper;


public class MyCustomExceptionHandlerResolver extends AbstractHandlerExceptionResolver{

	@Autowired
	ObjectMapper objectMapper;
	@Override
	protected ModelAndView doResolveException(HttpServletRequest request, HttpServletResponse response, Object handler,
			Exception ex) {
	//	if(ex instanceof MyCustomException)
			return null;
		
		
		/*Map<String, Object> responseMessage = new HashMap<String, Object>();
		response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
		responseMessage.put("timestamp",new Date());
		responseMessage.put("data", null);
		responseMessage.put("isSuccess",false);
		responseMessage.put("message","Sorry, site is busy please try again later");
		response.setContentType(MediaType.APPLICATION_JSON_VALUE);
		response.setCharacterEncoding(StandardCharsets.UTF_8.toString());
		try {
			String json = objectMapper.writeValueAsString(responseMessage);
			response.getWriter().write(json);
		} catch (Exception e) {
			logger.error("exception ",e);
		}

		return new ModelAndView();*/
	}

}

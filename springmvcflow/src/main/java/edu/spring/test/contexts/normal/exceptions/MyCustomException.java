package edu.spring.test.contexts.normal.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.UNAUTHORIZED)
public class MyCustomException extends RuntimeException{

	private static final long serialVersionUID = -7520483141708026118L;
	
	public MyCustomException(String message){
		super(message);
	}

}

package edu.spring.test.contexts.root;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;

import edu.spring.test.shared.services.TokenService;

@Configuration
@Order(value=1)
public class RootContextConfiguration {

	@Bean(name="tokenService",initMethod="init")
	public TokenService tokenService(){
		TokenService service=new TokenService();
		service.setContainerName("root");
		return service;
	}
}

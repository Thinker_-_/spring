package edu.spring.test.contexts.root;

import org.springframework.web.context.AbstractContextLoaderInitializer;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;

public class RootContextInitilizer extends AbstractContextLoaderInitializer{

	@Override
	protected WebApplicationContext createRootApplicationContext() {
		AnnotationConfigWebApplicationContext rootContext
		= new AnnotationConfigWebApplicationContext();
		rootContext.register(RootContextConfiguration.class);
		return rootContext;
	}

}

package edu.spring.test.contexts.normal.exceptions;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler {
	private static final Logger execptionlogger = LoggerFactory.getLogger(GlobalExceptionHandler.class);

	@ExceptionHandler(value=NullPointerException.class)
	public ResponseEntity<Object> nullPointerExceptionHandler(NullPointerException e){
		execptionlogger.error(e.getMessage(), e);
		Map<String,Object> map=new HashMap<String,Object>(6);
		map.put("isSuccess",false);
		map.put("data",null);
		map.put("status",HttpStatus.INTERNAL_SERVER_ERROR);
		map.put("message", e.getMessage());
		map.put("timestamp",new Date());
		map.put("fielderror",null);
		return new ResponseEntity<Object>(map,HttpStatus.INTERNAL_SERVER_ERROR);
	}
}

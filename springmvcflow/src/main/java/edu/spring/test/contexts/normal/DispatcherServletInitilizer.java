package edu.spring.test.contexts.normal;

import javax.servlet.MultipartConfigElement;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

public class DispatcherServletInitilizer implements WebApplicationInitializer {

	public void onStartup(ServletContext servletCxt) throws ServletException {
		AnnotationConfigWebApplicationContext webCtx = new AnnotationConfigWebApplicationContext();

		webCtx.register(NormalContextConfiguration.class);

		webCtx.setServletContext(servletCxt);
		DispatcherServlet dServlet= new DispatcherServlet(webCtx);
		dServlet.setDetectAllHandlerExceptionResolvers(false);
		dServlet.setThrowExceptionIfNoHandlerFound(true);
		ServletRegistration.Dynamic servlet = servletCxt.addServlet("dispatcher",dServlet);
		MultipartConfigElement multipartConfigElement=new MultipartConfigElement("/tmp", 1024*1024*2, 1024*1024*5, 1024*1024*1);
		servlet.setMultipartConfig(multipartConfigElement);
		servlet.setInitParameter("enableLoggingRequestDetails", "true");
		servlet.setLoadOnStartup(1);
		servlet.addMapping("/");
	}
}



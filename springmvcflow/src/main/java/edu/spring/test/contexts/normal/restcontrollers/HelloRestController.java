package edu.spring.test.contexts.normal.restcontrollers;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.BeanNameAware;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.DispatcherServlet;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.handler.HandlerExceptionResolverComposite;

import edu.spring.DAO.ResponseObject;
import edu.spring.test.shared.services.TokenService;



@RestController
public class HelloRestController implements BeanNameAware{

	@Autowired
	TokenService tokenService;

	@Autowired
	WebApplicationContext context;

	@RequestMapping(value="/login",method=RequestMethod.GET,produces="application/json")
	public ResponseEntity<Object> generateLoginToken(){
		ResponseObject object=new ResponseObject("logintoken",tokenService.getToken());
		Map<String,Object> map=new HashMap<String,Object>();
		map.put("isSuccess",true);
		map.put("data",object);
		map.put("status",HttpStatus.OK);
		map.put("message", "LoginSuccess");
		map.put("timestamp",new Date());
		map.put("fielderror",null);
		System.out.println("container name : "+tokenService.getContainerName());
		boolean var=true;

		HandlerExceptionResolver res=context.getBean(DispatcherServlet.HANDLER_EXCEPTION_RESOLVER_BEAN_NAME, HandlerExceptionResolver.class);
		if(res instanceof HandlerExceptionResolverComposite){
			HandlerExceptionResolverComposite resolver=(HandlerExceptionResolverComposite)context.getBean(DispatcherServlet.HANDLER_EXCEPTION_RESOLVER_BEAN_NAME, HandlerExceptionResolver.class);
			List<HandlerExceptionResolver> resolversInList=resolver.getExceptionResolvers();

			for(HandlerExceptionResolver resolvers:resolversInList){
				System.out.println(resolvers.getClass());
			}
		}
		System.out.println("class name is "+res.getClass());

		return new ResponseEntity<Object>(map,HttpStatus.OK);
	}
	


	@RequestMapping(value="/upload",method=RequestMethod.POST,produces="application/json")
	public ResponseEntity<Object> uploadFile(@RequestPart MultipartFile file1,@RequestPart  MultipartFile file2){

		Map<String,Object> object=new HashMap<String,Object>();
		object.put("file1",file1.getOriginalFilename());
		object.put("file2",file2.getOriginalFilename());
		
		
		try {
			FileCopyUtils.copy(file1.getBytes(), new File("/tmp/files/"+ file1.getOriginalFilename()));
			FileCopyUtils.copy(file2.getBytes(), new File("/tmp/files/"+ file2.getOriginalFilename()));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
		
		return new ResponseEntity<Object>(object,HttpStatus.OK);
	}

	public void setBeanName(String arg0) {
		System.out.println("My first rest controller init");

	}

}

package edu.spring.test.contexts.normal.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import edu.spring.test.shared.services.TokenService;

@Component
public class MessageBrokerService {
	
	@Autowired
	TokenService service;

	public TokenService getService() {
		return service;
	}

	public void setService(TokenService service) {
		this.service = service;
	}
	
}

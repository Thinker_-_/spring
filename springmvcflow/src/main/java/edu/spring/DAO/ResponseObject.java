package edu.spring.DAO;

public class ResponseObject {

	String responseKey;
	
	String responseValue;

	public String getResponseKey() {
		return responseKey;
	}

	public void setResponseKey(String responseKey) {
		this.responseKey = responseKey;
	}

	public String getResponseValue() {
		return responseValue;
	}

	public void setResponseValue(String responseValue) {
		this.responseValue = responseValue;
	}

	public ResponseObject(String responseKey, String responseValue) {
		super();
		this.responseKey = responseKey;
		this.responseValue = responseValue;
	}
	
	

}
